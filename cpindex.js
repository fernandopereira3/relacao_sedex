const express = require('express');
const bodyParser = require('body-parser');
const pdf = require('html-pdf');
const cors = require('cors');

// const pdfTemplate = require('./documents');

const app = express();

const port = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/create-pdf', (req, res) => {
  // console.log(req.body.rastreio);
  pdf.create(makePdf(req.body.rastreio, req.body.setor), {}).toFile('relacao_sedex.pdf', (err) => {
    if (err) {
      res.send(Promise.reject());
    }
    res.send(Promise.resolve());
  });
});

app.get('/fetch-pdf', (req, res) => {
  res.sendFile(`${__dirname}/relacao_sedex.pdf`)
})

app.listen(port, () => console.log(`Listening on port ${port}`));


const makePdf = (rastreio, setor) => {
  const today = new Date();
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  // options.timeZone = 'UTC';
  // options.timeZoneName = 'short';
  // Intl.DateTimeFormat('en-US', options).format(date)

  let html = ` <!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <title>PDF Result Template</title>
      <style>
         .invoice-box {
         max-width: 450px;
         margin: auto;
         padding: 30px;
         border: 2px solid #808080;
         box-shadow: 0 0 10px rgba(0, 0, 0, .15);
         font-size: 10px;
         line-height: 10px;
         font-family: 'Helvetica Neue', 'Helvetica',
         color: black;
         }
         .margin-top {
         margin-top: 25px;
         }
         .justify-center {
         text-align: right;
         }
         .invoice-box table {
         width: 100%;
         line-height: inherit;
         text-align: left;
         }
         .invoice-box table td {
         padding: 5px;
         vertical-align: top;
         }
         .invoice-box table tr td:nth-child(2) {
         text-align: right;
         }
         .invoice-box table tr.top table td {
         padding-bottom: 20px;
         }
         .invoice-box table tr.top table td.title {
         aling: right;
         font-size: 45px;
         line-height: 45px;
         color: #333;
         }
         .invoice-box table tr.information table td {
         padding-bottom: 40px;
         }
         .invoice-box table tr.heading td {
         max-width: 100px;
         background: #eee;
         border-bottom: 1px solid #ddd;
         font-weight: bold;
         }
         .invoice-box table tr.details td {
         padding-bottom: 20px;
         }
         .invoice-box table tr.item td {
         border-bottom: 1px solid #eee;
         }
         .invoice-box table tr.item.last td {
         border-bottom: none;
         }
         .invoice-box table tr.total td:nth-child(2) {
         border-top: 2px solid #eee;
         font-weight: bold;
         }
         @media only screen and (max-width: 600px) {
         .invoice-box table tr.top table td {
         width: 100%;
         display: block;
         text-align: center;
         }
         .invoice-box table tr.information table td {
         width: 100%;
         display: block;
         text-align: center;
         }
         .invoice-box table head{
           background-color: red;

         }
         }
      </style>
   </head>
   <body>
      <div class="invoice-box">
         <table cellpadding="0" cellspacing="0">
            <tr class="top">
               <td colspan="1">
                     <tr>
                        <td class="title"><img  src="https://lh3.googleusercontent.com/proxy/J7mfpMb1uQFyCRlTX5xzAxHnGCwzbQ7MHf5B9LWh3-28ZkrFT8oqU6qvQtUA1zNgPMx8iqZtQUlL9Df-mfcrgiaw46HUx3PJSnxLlLsG0fhONPMr7ZtUMGW6x2M2j0REw2Jk"
                              style="width:30%;" align: right; />
                        </td>     
                     </tr>
               </td>
          </table>
            </tr>
         <table class="head">
              <tr class="heading" colspan="1">
              <p>Relação de números de sedex e cartas registradas entregues ao setor: Rol de Visitas</p>
                <th>Número</th>
              </tr>`;
                    for (let i = 0; i < rastreio.length; i++) {
                      html = html +
                        `<tr class="item"  colspan="2">
                          <td>${i + 1}</td>
                          <td>${rastreio[i]}</td>
                        </tr>`;
                    };
                    html = html + `</table>
              <br />
            <p>Recebido por:____________________________________________________</p>
            <p> Pacaembu, ${Intl.DateTimeFormat('pt-BR', options).format(today)}</p>
          </table>
         </body >
         </html >
  `;
  return html;
  //  <p> Pacaembu, ${`${today.getDate()} de ${today.getMonth() + 1} de ${today.getFullYear()}`}</p>
}