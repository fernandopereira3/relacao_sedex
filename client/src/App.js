import React, { useState } from 'react';
import axios from 'axios';
import { saveAs } from 'file-saver';
import './styles.css';

const App = () => {
  const [rastreio, setRastreio] = useState("");
  const [listaRastreio, setListaRastreio] = useState([]);
 // const [setor, setSetor] = useState("");

  const handleChange = (value) => {
    setRastreio(value);
  //  setSetor(value);
  }

  const handleKeyDown = (keyCode) => {
    if (keyCode === 13) {
      setListaRastreio([...listaRastreio, rastreio]);
      setRastreio("");
    }
  }

  const createAndDownloadPdf = () => {
    axios.post('/create-pdf', { "rastreio": listaRastreio})
      .then(() => axios.get('fetch-pdf', { responseType: 'blob' }))
      .then((res) => {
        const pdfBlob = new Blob([res.data], { type: 'application/pdf' });
        saveAs(pdfBlob, 'Relacao Sedex.pdf');
      })
  }
  return (
    <div className="App">
      <div className="content">
        <h1>RELAÇÃO DE SEDEX</h1>
        <label>Setor:</label>
        <input type="text" placeholder="Rol de Visitas" name="setor"
 //         value={setor}
 //         onChange={e => handleChange(e.target.value)}
        />
        <label>Códido de Rastreio:</label>
        <input type="text" placeholder="Ex: AA123456789BR" name="codRastreio"
          value={rastreio}
          onKeyDown={e => handleKeyDown(e.keyCode)}
          onChange={e => handleChange(e.target.value)}
        />
        <button className="imprimir" onClick={createAndDownloadPdf}>Gerar Relação</button>
        <label />
        {<tr>
        {!!listaRastreio && listaRastreio.map(value => (<th id="Lista"><p>{value}        ,</p></th>))}
        </tr> }
      </div>
    </div>
  );
};
export default App;