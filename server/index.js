const express = require('express');
const bodyParser = require('body-parser');
const pdf = require('html-pdf');
const cors = require('cors');

// const pdfTemplate = require('./documents');

const app = express();

const port = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/create-pdf', (req, res) => {
 // console.log(req.body.rastreio);
 // console.log(req.body.setor);
  pdf.create(makePdf(req.body.rastreio, req.body.setor), {}).toFile('relacao_sedex.pdf', (err) => {
    if (err) {
      res.send(Promise.reject());
    }
    res.send(Promise.resolve());
  });
});

app.get('/fetch-pdf', (req, res) => {
  res.sendFile(`${__dirname}/relacao_sedex.pdf`)
})

app.listen(port, () => console.log(`Listening on port ${port}`));


const makePdf = (rastreio, setor) => {
  const today = new Date();
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  // options.timeZone = 'UTC';
  // options.timeZoneName = 'short';
  // Intl.DateTimeFormat('en-US', options).format(date)

  let html = ` <!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <title>PDF Result Template</title>
      <style>
            .invoice-box {
              display: flex;
              flex-direction: column;
              width: 100%;
              margin: auto;
              border: 1px solid black;
              font-size: 10px;
              line-height: 10px;
              color: black;
              padding: 1px;
              }

            .invoice-box tr.head{
              display: inline-block;
              width: 100%;

            }

            .invoice-box table.tabela{
              margin: auto;
              align-items: center;
            }

            .invoice-box tr.item{
              
            }

       
      </style>
   </head>
   <body>
      <div class="invoice-box">
         <table cellpadding="0" cellspacing="0">
            <tr class="head">
               <td colspan="1">
                     <tr>
                        <td class="title"><img  src="https://lh3.googleusercontent.com/proxy/J7mfpMb1uQFyCRlTX5xzAxHnGCwzbQ7MHf5B9LWh3-28ZkrFT8oqU6qvQtUA1zNgPMx8iqZtQUlL9Df-mfcrgiaw46HUx3PJSnxLlLsG0fhONPMr7ZtUMGW6x2M2j0REw2Jk"
                              style="width:30%;" align: right; />
                        </td>     
                     </tr>
               </td>
          </table>
            </tr>
         <table class="tabela">
              <tr class="heading" colspan="1">
              <p>Relação de números de sedex e cartas registradas entregues ao setor: Rol de Visitas</p>
              </tr>`
                    ;
                    for (let i = 0; i < rastreio.length; i++) {
                      html = html +
                        `<tr class="item"  colspan="1">
                          <td>${i + 1} -------------- ${rastreio[i]}</td>
                        </tr>`;
                    };
                    html = html + `</table>
            <br />
            <p>Recebido por:____________________________________________________</p>
            <p> Pacaembu, ${Intl.DateTimeFormat('pt-BR', options).format(today)}</p>
          </table>
         </body >
         </html >
  `;
  return html;
  //  <p> Pacaembu, ${`${today.getDate()} de ${today.getMonth() + 1} de ${today.getFullYear()}`}</p>
}